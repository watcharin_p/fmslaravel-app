<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'employee_id',
        'prename_full_tha',
        'first_name_tha',
        'last_name_tha',
        'organization_parent',
        'organization_name_tha',
        'position_rank_name',
        'picture'
    ];
}
