<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResearchDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'employee_id',
        'research_project_id',
        'project_continue_type_name_tha',
        'research_project_name_tha',
        'project_research_member_action_name_tha'
    ];
}
