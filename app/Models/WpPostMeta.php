<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WpPostMeta extends Model
{
    protected $connection = 'wordpress_db'; // เชื่อมต่อกับฐานข้อมูล WordPress
    protected $table = 'wp_postmeta'; // ตาราง wp_postmeta
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    protected $fillable = [
        'post_id',
        'meta_key',
        'meta_value'
    ];
}
