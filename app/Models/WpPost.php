<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WpPost extends Model
{
    protected $connection = 'wordpress_db'; // เชื่อมต่อกับฐานข้อมูล wordpress_db
    protected $table = 'wp_posts'; // ตาราง wp_posts
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function terms()
    {
        return $this->belongsToMany(
            WpTerm::class,
            'wp_term_relationships',
            'object_id',            // คอลัมน์ใน wp_term_relationships ที่เชื่อมโยงกับ wp_posts
            'term_taxonomy_id'       // คอลัมน์ใน wp_term_relationships ที่เชื่อมโยงกับ wp_term_taxonomy
        )
            ->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id', '=', 'wp_term_relationships.term_taxonomy_id')
            ->select('wp_term_taxonomy.*'); // เลือกข้อมูลจาก wp_term_taxonomy
    }
}
