<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $connection = 'laravel_db'; // เชื่อมต่อกับฐานข้อมูล laravel_db
    protected $table = 'posts'; // ตาราง posts ของ Laravel
    public $timestamps = true;

    protected $fillable = [
        'title',
        'content'
    ];
}
