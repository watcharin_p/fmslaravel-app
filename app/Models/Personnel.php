<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Personnel extends Model
{
    protected $fillable = [
        'employee_id',
        'prename_full_tha',
        'first_name_tha',
        'last_name_tha',
        'employee_name',
        'office_phone',
        'work_status_name',
        'employee_type_name',
        'sunit_name',
        'position_rank_name',
        'picture',
        'bullet_link1',
        'bullet_link2',
        'bullet_link3',
        'bullet_link4',
    ];
    // ฟังก์ชันสำหรับดึงข้อมูลจาก API
    public static function fetchFromApi()
    {
        $apiUrl = 'https://mis.kpru.ac.th/api/EmployeeInOrg/004'; // URL ของ API
        $response = Http::get($apiUrl);

        if ($response->successful()) {
            $employees = $response->json();
            foreach ($employees as $data) {
                // บันทึกข้อมูลลงฐานข้อมูล
                self::updateOrCreate(
                    ['employee_id' => $data['employee_id']],
                    [
                        'prename_full_tha' => $data['prename_full_tha'],
                        'first_name_tha' => $data['first_name_tha'],
                        'last_name_tha' => $data['last_name_tha'],
                        'employee_name' => $data['employee_name'],
                        'office_phone' => $data['office_phone'],
                        'work_status_name' => $data['work_status_name'],
                        'employee_type_name' => $data['employee_type_name'],
                        'sunit_name' => $data['sunit_name'],
                        'position_rank_name' => $data['position_rank_name'],
                        'picture' => $data['picture']
                    ]
                );
            }
        }
    }
}
