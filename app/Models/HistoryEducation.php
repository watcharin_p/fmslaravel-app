<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryEducation extends Model
{
    use HasFactory;
    protected $fillable = [
        'employee_id',
        'institute_name',
        'curriculum_name_tha',
        'certificate_name',
        'program_name_th',
        'graduate_year',
        'education_level_name',
        'country_name_tha'
    ];
}
