<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WpTerm extends Model
{
    protected $connection = 'wordpress_db'; // ฐานข้อมูล WordPress
    protected $table = 'wp_terms'; // ตาราง terms
    protected $primaryKey = 'term_id';
    public $timestamps = false;
}
