<?php

namespace App\Models\WpPr;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $table = 'pr_activities';
    protected $fillable = [
        'api_id',
        'slug',
        'status',
        'link',
        'title',
        'excerpt',
        'featured_media',
        'formattedDate'
    ];
}
