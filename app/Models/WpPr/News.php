<?php

namespace App\Models\WpPr;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $table = 'pr_news';
    protected $fillable = [
        'api_id',
        'date',
        'slug',
        'status',
        'link',
        'title',
        'featured_media',
        'formattedDate'
    ];
}
