<?php

namespace App\Models\WpPr;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carousel extends Model
{
    use HasFactory;
    protected $table = 'pr_carousels';
    protected $fillable = [
        'api_id',
        'slug',
        'status',
        'link',
        'title',
        'featured_media'
    ];
}
