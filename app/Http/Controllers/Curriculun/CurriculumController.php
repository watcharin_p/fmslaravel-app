<?php

namespace App\Http\Controllers\Curriculun;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

class CurriculumController extends Controller
{
    // หน้า Under Graduate page
    public function undergraduate()
    {
        // return Inertia::render('Under-Graduate');
        return "Under Graduate";
    }
    //  หน้าหลักสูตร Accounting
    public function CurriculumAccounting()
    {
        return Inertia::render('Curriculum/Curriculum-Accounting');
    }
    //  หน้าหลักสูตร Business Administration
    public function CurriculumBusinessadministration()
    {
        return Inertia::render('Curriculum/Curriculum-Business-Administration');
    }
    //  หน้าหลักสูตร Business Management Major
    public function CurriculumBusinessManagementMajor()
    {
        return Inertia::render('Curriculum/Curriculum-Business-Management-Major');
    }
    //  หน้าหลักสูตร Marketing Major
    public function CurriculumMarketingMajor()
    {
        return Inertia::render('Curriculum/Curriculum-Marketing-Major');
    }
    //  หน้าหลักสูตร Finance And Banking Major
    public function CurriculumFinanceAndBankingMajor()
    {
        return Inertia::render('Curriculum/Curriculum-Finance-And-Banking-Major');
    }
    //  หน้าหลักสูตร Digital Business Technology Major
    public function CurriculumDigitalBusinessTechnologyMajor()
    {
        return Inertia::render('Curriculum/Curriculum-Digital-Business-Technology-Major');
    }
    //  หน้าหลักสูตร Entrepreneurship Major
    public function CurriculumEntrepreneurshipMajor()
    {
        return Inertia::render('Curriculum/Curriculum-Entrepreneurship-Major');
    }
    //  หน้าหลักสูตร Communication Arts
    public function CurriculumTourismIndustry()
    {
        return Inertia::render('Curriculum/Curriculum-Tourism-Industry');
    }
    //  หน้าหลักสูตร Communication Arts
    public function CurriculumCommunicationArts()
    {
        return Inertia::render('Curriculum/Curriculum-Communication-Arts');
    }


    // หน้า Master Graduate page
    public function mastergraduate()
    {
        // return Inertia::render('Master Graduate');
        return "Master Graduate";
    }
    //  หน้าหลักสูตร MBA
    public function CurriculumMBA()
    {

        return Inertia::render('Curriculum/Curriculum-MBA');
    }
}
