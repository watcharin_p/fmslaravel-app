<?php

namespace App\Http\Controllers;

use App\Services\TestService;

// use App\Models\WpPostMeta;
// use Illuminate\Http\Request;

class PostController extends Controller
{

    protected $testService;

    public function __construct(testService $testService)
    {
        $this->testService = $testService;
    }
    public function index()
    {
        $formattedData = $this->testService->fetchData();
        return response()->json($formattedData);
    }
}
