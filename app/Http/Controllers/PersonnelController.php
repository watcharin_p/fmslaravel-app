<?php

namespace App\Http\Controllers;

use Inertia\Inertia;

use App\Models\Personnel;
use App\Models\EmployeeDetail;
use App\Models\ResearchDetail;
use App\Models\AcademicDetail;
use App\Models\HistoryEducation;
use Illuminate\Support\Facades\Http;

class PersonnelController extends Controller
{
    public function index()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',

        )
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Personnel/Personnel', ['personnels' => $personnels]);
    }

    public function show($employee_id)
    {
        //ดึงข้อมูล EmployeeDetail
        $employeeDetail = EmployeeDetail::where('employee_id', $employee_id)->first();

        //ดึงข้อมูล ResearchDetaill
        $researchDetail = ResearchDetail::where('employee_id', $employee_id)->get();

        // ดึงข้อมูลจาก AcademicDetail
        $academicDetails = AcademicDetail::where('employee_id', $employee_id)->get();

        // ดึงข้อมูลจาก HistoryEducation
        $historyEducations = HistoryEducation::where('employee_id', $employee_id)->get();

        // ดึงข้อมูลจาก PhotoAlbum (ถ้ามีข้อมูลรูปภาพ)
        // $photoAlbum = PhotoAlbum::where('employee_id', $employee_id)->get();

        // ตรวจสอบว่ามีข้อมูลพนักงานหรือไม่
        if (!$employeeDetail) {
            return redirect()->back()->withErrors('ไม่พบข้อมูลพนักงาน');
        }

        return Inertia::render(
            'Personnel/Personnel-Show',
            [
                'employee' => $employeeDetail,
                'research' => $researchDetail,
                'academic' => $academicDetails,
                'educational' => $historyEducations
            ]

        );
    }

    public function fetchAndStore()
    {
        // ดึงข้อมูลจาก API และบันทึกลงฐานข้อมูล
        Personnel::fetchFromApi();

        return response()->json(['message' => 'Data fetched and stored successfully.']);
    }
    public function featchEmployeeDetail($employee_id)
    {
        // เรียก API โดยใช้ employee_id
        $response = Http::get("https://mis.kpru.ac.th/api/EmployeeDetailAPI/{$employee_id}");

        if ($response->successful()) {
            $data = $response->json();

            // เก็บข้อมูล EmpDetail ลงฐานข้อมูล
            $empDetail = $data['EmpDetail'][0];
            EmployeeDetail::updateOrCreate(
                ['employee_id' => $employee_id],
                [
                    'prename_full_tha' => $empDetail['prename_full_tha'],
                    'first_name_tha' => $empDetail['first_name_tha'],
                    'last_name_tha' => $empDetail['last_name_tha'],
                    'organization_parent' => $empDetail['organization_parent'],
                    'organization_name_tha' => $empDetail['organization_name_tha'],
                    'position_rank_name' => $empDetail['position_rank_name'],
                    'picture' => $empDetail['picture']
                ]
            );

            // เก็บข้อมูล ResearchDetail ลงฐานข้อมูล
            foreach ($data['ResearchDetail'] as $research) {
                ResearchDetail::updateOrCreate(
                    [
                        'employee_id' => $employee_id,
                        'research_project_id' => $research['research_project_id']
                    ],
                    [
                        'project_continue_type_name_tha' => $research['project_continue_type_name_tha'],
                        'research_project_name_tha' => $research['research_project_name_tha'],
                        'project_research_member_action_name_tha' => $research['project_research_member_action_name_tha']
                    ]
                );
            }

            // เก็บข้อมูล HistoryEducation ลงฐานข้อมูล
            foreach ($data['HistoryEducation'] as $education) {
                HistoryEducation::updateOrCreate(
                    [
                        'employee_id' => $employee_id,
                        'institute_name' => $education['institute_name']
                    ],
                    [
                        'curriculum_name_tha' => $education['curriculum_name_tha'],
                        'certificate_name' => $education['certificate_name'],
                        'program_name_th' => $education['program_name_th'],
                        'graduate_year' => $education['graduate_year'],
                        'education_level_name' => $education['education_level_name'],
                        'country_name_tha' => $education['country_name_tha']
                    ]
                );
            }
            // เก็บข้อมูล AcademicDetail ลงฐานข้อมูล
            if (!empty($data['AcademicDetail'])) {
                foreach ($data['AcademicDetail'] as $academic) {
                    AcademicDetail::updateOrCreate(
                        [
                            'employee_id' => $employee_id,
                            'research_project_id' => $academic['research_project_id']
                        ],
                        [
                            'publication_title' => $academic['publication_title'],
                            'publication_type_name_tha' => $academic['publication_type_name_tha'],
                            'publication_level_name_tha' => $academic['publication_level_name_tha'],
                            'county_name_tha' => $academic['county_name_tha'],
                            'member_action_percent' => $academic['member_action_percent'] ?? null,
                            'member_action_type_name_tha' => $academic['member_action_type_name_tha'] ?? null,
                            'publication_owner_all' => $academic['publication_owner_all'] ?? null
                        ]
                    );
                }
            }

            return 'Employee details and related data have been saved successfully.';
        }

        return 'Failed to fetch data from API.';
    }
}
