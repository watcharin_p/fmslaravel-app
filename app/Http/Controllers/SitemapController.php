<?php

namespace App\Http\Controllers;

use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class SitemapController extends Controller
{
    public function generateSitemap()
    {
        $sitemap = Sitemap::create();

        $sitemap->add(
                Url::create('/')
                    ->setPriority(1.0)
                    ->setChangeFrequency('daily')
                    ->setLastModificationDate(now())
                    ->addVideo(
                        'https://fms.kpru.ac.th/videos/video-thumbnail.jpg',
                        'แนะนำหลักสูตรคณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร',
                        'FMS KPRU แนะนำหลักสูตรคณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร',
                        'https://fms.kpru.ac.th/videos/fms-vdo.mp4',
                    )
        );
        $urls = [
            '/pr-fms' => ['priority' => 0.9, 'changefreq' => 'daily'],
            '/about' => ['priority' => 0.8, 'changefreq' => 'monthly'],
            '/undergraduate' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/accounting' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/business-administration' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/business-management-major' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/marketing-major' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/finance-and-banking-major' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/digital-business-technology-major' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/entrepreneurship-major' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/tourism-industry' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/undergraduate/communication-arts' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/mastergraduate' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/mastergraduate/mba' => ['priority' => 0.8, 'changefreq' => 'yearly'],
            '/service' => ['priority' => 0.9, 'changefreq' => 'monthly'],
            '/contact-us' => ['priority' => 0.9, 'changefreq' => 'yearly'],
            '/accounting'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/business-management-major' => ['priority' => 0.6,'changefreq' => 'monthly'],
            '/marketing-major'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/finance-and-banking-major'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/digital-business-technology-major'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/entrepreneurship-major'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/tourism-industry'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/communication-arts'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/mba'=> ['priority' => 0.6,'changefreq' => 'monthly'],
            '/research-innovation'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            '/academic-services'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            '/plan-develop'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            '/academic-work'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            '/knowledge-management'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            '/student-affairs'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            '/apprentice-services'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            '/development-human-resources'=> ['priority' => 0.5,'changefreq' => 'monthly'],
            // ->writeToFile(public_path('sitemap.xml'));
        ];
        foreach ($urls as $url => $attributes) {
            $sitemap->add(
                Url::create($url)
                    ->setPriority($attributes['priority'])
                    ->setChangeFrequency($attributes['changefreq'])
                    ->setLastModificationDate(now())
            );
        }
        // บันทึก Sitemap ลงไฟล์
        $sitemap->writeToFile(public_path('sitemap.xml'));

        return response()->json(['message' => 'Sitemap generated successfully!']);
    }
}
