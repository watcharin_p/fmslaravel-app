<?php

namespace App\Http\Controllers;

// Models
use App\Models\WpPr\Activity;
use App\Models\WpPr\Carousel;
use App\Models\WpPr\News;

// Supports
use Inertia\Inertia;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{


    public function __construct()
    {
        //
    }

    // หน้า landing page
    public function index()
    {
        $items = [
            (object) ['modal' => "accountingModal",  'color' => "red-600", 'image' => "/images/poster-accounting.webp", 'linkweb' => "accounting", 'curriculum' => "undergraduate/accounting", 'alt' => "Accounting , หลักสูตรบัญชีบัณฑิต สาขาวิชาการบัญชี"],

            (object) ['modal' => "bmmModal", 'color' => "[#ff5722]",  'image' => "/images/poster-business-management-major.webp", 'linkweb' => "business-management-major", 'curriculum' => "/undergraduate/business-management-major", 'alt' => "Business Management Major , หลักสูตรบริหารธุรกิจบัณฑิต วิชาเอกการจัดการธุรกิจ"],

            (object) ['modal' => "mkmModal", 'color' => "[#0097db]", 'image' => "/images/poster-marketing-major.webp", 'linkweb' => "marketing-major", 'curriculum' => "/undergraduate/marketing-major", 'alt' => "Marketing Major , หลักสูตรบริหารธุรกิจบัณฑิต วิชาเอกการตลาด"],

            (object) ['modal' => "fabmModel", 'color' => "[#0000ff]",  'image' => "/images/poster-finance-and-banking-major.webp", 'linkweb' => "finance-and-banking-major", 'curriculum' => "/undergraduate/finance-and-banking-major", 'alt' => "Finance And Banking Major , หลักสูตรบริหารธุรกิจบัณฑิต วิชาเอกการเงินและการธนาคาร"],

            (object) ['modal' => "dbtmModel", 'color' => "[#000066]", 'image' => "/images/poster-digital-business-technology-major.webp", 'linkweb' => "digital-business-technology-major", 'curriculum' => "/undergraduate/digital-business-technology-major", 'alt' => "Digital Business Technology Major , หลักสูตรบริหารธุรกิจบัณฑิต วิชาเอกเทคโนโลยีธุรกิจดิจิทัล"],

            (object) ['modal' => "emModel", 'color' => "[#ff6666]", 'image' => "/images/poster-entrepreneurship-major.webp", 'linkweb' => "entrepreneurship-major", 'curriculum' => "/undergraduate/entrepreneurship-major", 'alt' => "Entrepreneurship Major , หลักสูตรบริหารธุรกิจบัณฑิต วิชาเอกการเป็นผู้ประกอบการ"],

            (object) ['modal' => "tiModel", 'color' => "purple-600", 'image' => "/images/poster-tourism-industry.webp", 'linkweb' => "tourism-industry", 'curriculum' => "/undergraduate/tourism-industry", 'alt' => "Tourism Industry , หลักสูตรศิลปศาสตรบัณฑิต สาขาวิชาอุตสาหกรรมการท่องเที่ยว"],

            (object) ['modal' => "caModel", 'color' => "pink-600", 'image' => "/images/poster-communication-arts.webp", 'linkweb' => "communication-arts", 'curriculum' => "/undergraduate/communication-arts", 'alt' => "Communication Arts , หลักสูตรนิเทศศาสตรบัณฑิต สาขาวิชานิเทศศาสตร์"],

            (object) ['modal' => "mbaModel", 'color' => "pink-500", 'image' => "/images/poster-mba.webp", 'linkweb' => "mba", 'curriculum' => "/mastergraduate/mba", 'alt' => "MBA , หลักสูตรบริหารธุรกิจมหาบัณฑิต สาขาวิชาการจัดการสมัยใหม่"],
        ];

        return Inertia::render('Welcome', ['items' => $items]);
    }

    // หน้า main page
    public function home()
    {
        // ดึงข้อมูลล่าสุด 10 ชุดจากแต่ละโมเดล
        try {
            // แคชข้อมูล Carousel
            $carousels = Cache::remember('carousel_data', 300, function () {
                return Carousel::orderBy('created_at', 'desc')->take(10)->get();
            });

            // แคชข้อมูล News
            $news = Cache::remember('news_data', 300, function () {
                return News::orderBy('created_at', 'desc')->take(12)->get();
            });

            // แคชข้อมูล Activity
            $activities = Cache::remember('activitie_data', 300, function () {
                return Activity::orderBy('created_at', 'desc')->take(10)->get();
            });

            // ส่งข้อมูลไปยังหน้า Inertia
            return Inertia::render('Home', [
                'carousels' => $carousels,
                'news' => $news,
                'activities' => $activities
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'ไม่สามารถดึงข้อมูลได้'], 500);
        }
    }

    // หน้าเกี่ยวกับเรา
    public function about()
    {
        return Inertia::render('About');
    }
    //  หน้าบริการ
    public function service()
    {
        return Inertia::render('Services/Service');
    }
    public function fmsbooking()
    {
        return Inertia::render('Services/FMSBooking');
    }

    //  หน้าติดต่อเรา
    public function contact()
    {
        return Inertia::render('ContactUs');
    }

    // หน้า วิจัย
    public function Research()
    {
        return "วิจัย คณะวิทยาการจัดการ";
        // return Inertia::render('FMS-Research');
    }

    // หน้า วารสาร
    public function Journal()
    {
        // return "วารสารคณะวิทยาการจัดการ";
        return Inertia::render('FMS-Journal');
    }

    // หน้า แนะนำ
    public function Introduction()
    {
        // return "introduction";
        return Inertia::render('Introduction/Index');
    }
    // หน้า แนะนำ มหาวิทยาลัย
    public function University()
    {
        // return "university";
        return Inertia::render('Introduction/University');
    }
    // หน้า แนะนำ คณะ
    public function Faculty()
    {
        // return "faculty";
        return Inertia::render('Introduction/Faculty');
    }
    // หน้า แนะนำ บรรยากาศมหาวิทยาลัย
    public function UniversityAtmosphere()
    {
        // return "University Atmosphere";
        return Inertia::render('Introduction/University-Atmosphere');
    }
    // หน้า แนะนำ บรรยากาศคณะ
    public function FacultyAtmosphere()
    {
        // return "Faculty Atmosphere";
        return Inertia::render('Introduction/Faculty-Atmosphere');
    }
}
