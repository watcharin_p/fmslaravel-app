<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class EventController extends Controller
{
    public function index()
    {
        //Event ไปหน้า home
        return redirect(route('home'));
    }

    public function NACM14th()
    {
        return Inertia::render('Event/NACM14th');
    }

    public function rovfms()
    {
        return Inertia::render('Event/NACM14th/rovfms');
    }
    public function openingnacm14th()
    {
        return Inertia::render('Event/NACM14th/opening-nacm14th');
    }
    public function TheStorytellingMasterAward()
    {
        return Inertia::render('Event/NACM14th/TheStorytellingMasterAward');
    }
}
