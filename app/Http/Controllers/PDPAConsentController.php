<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PDPAConsentController extends Controller
{
    public function showConsentForm()
    {
        // return view('pdpa.consent');
    }

    public function acceptConsent(Request $request)
    {
        // เก็บการยอมรับไว้ใน cookie และ redirect กลับไปที่หน้าเดิม
        return redirect()->back()->withCookie(cookie()->forever('pdpa_consent', true, 60 * 24 * 365));
    }
    public function resetConsent(Request $request)
    {
        // ลบคุกกี้ PDPA และรีเฟรชหน้า
        return redirect()->back()->withCookie(Cookie()->forget('pdpa_consent'));
    }
}
