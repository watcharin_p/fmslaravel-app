<?php
// app/Http/Controllers/ComplaintController.php

namespace App\Http\Controllers;


use App\Services\RecaptchaService;
use App\Models\Complaint;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ComplaintController extends Controller
{
    protected $recaptcha;

    public function __construct(RecaptchaService $recaptcha)
    {
        $this->recaptcha = $recaptcha;
    }
    public function index()
    {

        return view('Dean');
    }

    public function store(Request $request)
    {
        // Validate ฟอร์ม
        $request->validate([
            '_token' => 'required|string|max:255',
            'fullname' => 'required|string|max:255',
            'phone' => 'required|numeric|digits:10',
            'email' => 'required|email|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string',
            'g-recaptcha-response' => 'required',
        ]);
        $g_response = Http::asForm()->post("https://www.google.com/recaptcha/api/siteverify", [
            'secret' => config('captcha.secret'),
            'response' => $request->input('g-recaptcha-response'),
            'remoteip' => request()->ip(),
        ]);
        $result = $g_response->json();
        if ($result['success'] === true) {
            //บันทึกข้อมูลลงฐานข้อมูล
            Complaint::create([
                'fullname' => $request->input('fullname'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'subject' => $request->input('subject'),
                'message' => $request->input('message'),
            ]);
            // dump($request);
            // ส่งผลตอบกลับไปยังผู้ใช้
            return "reCAPTCHA ผ่านบันทึกข้อมูลสำเร็จ";
        } else {
            // หากการตรวจสอบ reCAPTCHA ไม่ผ่าน
            return "reCAPTCHA ไม่ผ่าน";
        }


        // ส่งผลตอบกลับไปยังผู้ใช้
        //     return redirect()->back()->with('success', 'Complaint has been submitted successfully!');
        // } else {
        // หากการตรวจสอบ reCAPTCHA ไม่ผ่าน
        //     return redirect()->back()->with('error', 'reCAPTCHA verification failed. Please try again.');
        // }

        // Complaint::create($request->all());
        // return redirect()->route('dean')->with('success', 'Your complaint has been submitted!');
        // }
        // Complaint::create($request->all());
        // return redirect()->route('dean')->with('success', 'Your complaint has been submitted!');
    }
    public function view()
    {
        // $complaint
        return Complaint::orderBy('created_at', 'desc')->take(10)->get();
    }
}
