<?php

namespace App\Http\Controllers\Department;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

use App\Models\Personnel;

class DepartmentController extends Controller
{
    // แผนกสาขาวิชา
    public function Accounting()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาการบัญชี')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/Accounting', ['personnels' => $personnels]);
    }
    public function BusinessManagementMajor()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาบริหารธุรกิจ วิชาเอกการจัดการธุรกิจ')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/BusinessManagementMajor', ['personnels' => $personnels]);
    }
    public function MarketingMajor()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาบริหารธุรกิจ วิชาเอกการตลาด')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/MarketingMajor', ['personnels' => $personnels]);
    }
    public function FinanceAndBankingMajor()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาบริหารธุรกิจ วิชาเอกการเงินและการธนาคาร')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/FinanceAndBankingMajor', ['personnels' => $personnels]);
    }
    public function DigitalBusinessTechnologyMajor()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาบริหารธุรกิจ วิชาเอกเทคโนโลยีธุรกิจดิจิทัล')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/DigitalBusinessTechnologyMajor', ['personnels' => $personnels]);
    }
    public function EntrepreneurshipMajor()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาบริหารธุรกิจ วิชาเอกเป็นผู้ประกอบการ')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/EntrepreneurshipMajor', ['personnels' => $personnels]);
    }
    public function TourismIndustry()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาอุตสาหกรรมการท่องเที่ยว')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/TourismIndustry', ['personnels' => $personnels]);
    }
    public function CommunicationArts()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชานิเทศศาสตร์')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/CommunicationArts', ['personnels' => $personnels]);
    }
    public function MBA()
    {
        $personnels = Personnel::select(
            'id',
            'employee_id',
            'first_name_tha',
            'last_name_tha',
            'sunit_name',
            'position_rank_name',
            'picture',
            'bullet_link1',
            'bullet_link2',
            'bullet_link3',
            'bullet_link4',
        )
            ->where('sunit_name', 'สาขาวิชาการจัดการธุรกิจสมัยใหม่')
            ->where('position_rank_name', '!=', 'อาจารย์รายชั่วโมง')
            ->get();
        return Inertia::render('Department/MBA', ['personnels' => $personnels]);
    }

    // แผนกหน่วยงานย่อย

    public function ResearchInnovation()
    {

        return Inertia::render('Department/ResearchInnovation');
    }
    public function AcademicServices()
    {

        return Inertia::render('Department/AcademicServices');
    }
    public function PlanDevelop()
    {

        return Inertia::render('Department/PlanDevelop');
    }
    public function AcademicWork()
    {

        return Inertia::render('Department/AcademicWork');
    }
    public function KnowledgeManagement()
    {

        return Inertia::render('Department/KnowledgeManagement');
    }
    public function StudentAffairs()
    {

        return Inertia::render('Department/StudentAffairs');
    }

    public function InternshipServices()
    {

        return Inertia::render('Department/InternshipServices');
    }

    public function HumanResourceDevelopment()
    {

        return Inertia::render('Department/HRDC/HumanResourceDevelopment');
    }
}
