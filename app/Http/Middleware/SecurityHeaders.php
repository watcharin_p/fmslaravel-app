<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

class SecurityHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $nonce = View::shared('nonce');

        $response = $next($request);

        // ตรวจสอบว่าเป็น HTML response
        if ($response instanceof Response && $response->headers->get('content-type') === 'text/html; charset=UTF-8') {
            // เพิ่ม nonce ให้กับทุก <script>
            $content = $response->getContent();
            $content = str_replace('<script', '<script nonce="' . $nonce . '"', $content);

            // อัปเดตเนื้อหาของ response
            $response->setContent($content);
        }

        // ตั้งค่า Security Headers

        $response->headers->set(
            'Content-Security-Policy',
            "script-src 'self' 'nonce-$nonce' https://apis.google.com/ https://www.google-analytics.com/ https://www.googletagmanager.com/ https://fms2.kpru.ac.th/  https:; img-src 'self' http://www.kpru.ac.th/ https://fms.kpru.ac.th/ https://fms2.kpru.ac.th/  https://mis.kpru.ac.th/ https://arit.kpru.ac.th/ https://www.googletagmanager.com/ https://flowbite.s3.amazonaws.com/ https://cookiecdn.com/ https://flowbite.com/ data:;"

        );
        // $response->headers->set('X-Frame-Options', 'SAMEORIGIN');
        // $response->headers->set('X-Content-Type-Options', 'nosniff');
        // $response->headers->set('Referrer-Policy', 'strict-origin-when-cross-origin');
        // $response->headers->set('Permissions-Policy', 'geolocation=(self), microphone=(),camera=(self)');


        // แชร์ nonce ไปยัง view
        // view()->share('nonce', $nonce);

        return $response;
    }
}
