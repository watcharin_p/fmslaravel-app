<?php

namespace App\Jobs;

use App\Services\CarouselService;
use App\Models\WpPr\Carousel;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Facades\Log;

class FetchCarouselJob implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    protected $carouselService;

    public function __construct(CarouselService $carouselService)
    {
        // กำหนดค่าให้ carouselService
        $this->carouselService = $carouselService;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $carouselService = app(CarouselService::class);
            $carouselData = $carouselService->fetchData();

            // วนลูปข้อมูลแล้วบันทึกลงฐานข้อมูล
            foreach ($carouselData as $item) {
                Carousel::updateOrCreate(
                    ['api_id' => $item['id']],
                    [
                        'slug' => $item['slug'],
                        'status' => $item['status'],
                        'link' => $item['link'],
                        'title' => $item['title']['rendered'],
                        'featured_media' => $item['guid']['rendered']
                    ]
                );
                Log::info('Preparing to save CarouselJob: ', $item);
            }
        } catch (\Exception $e) {
            Log::info("message");
            ('Error in ProcessCarouselJob: ' . $e->getMessage());
        }
    }
}
