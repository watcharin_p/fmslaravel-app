<?php

namespace App\Jobs;

use App\Services\ActivitieService;
use App\Models\WpPr\Activity;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Facades\Log;

class FetchActivitieJob implements ShouldQueue
{
    use Queueable;

    protected $activitieService;

    public function __construct(ActivitieService $activitieService)
    {
        $this->activitieService = $activitieService;
    }

    public function handle(): void
    {
        try {
            $activitieService = app(ActivitieService::class);
            $activityData = $activitieService->fetchData();
            Log::info('Fetched activity data: ', $activityData);
            Log::info('Job started');
            // หลังจากที่ได้ข้อมูล
            Log::info('Data fetched');
            // ก่อนการบันทึก
            Log::info('Saving to database');
            // วนลูปข้อมูลแล้วบันทึกลงฐานข้อมูล
            foreach ($activityData as $item) {
                Activity::updateOrCreate(
                    ['api_id' => $item['id']], // เช็คข้อมูลตาม id จาก API
                    [
                        'slug' => $item['slug'],
                        'status' => $item['status'],
                        'link' => $item['link'],
                        'title' => $item['title']['rendered'],
                        'excerpt' => $item['excerpt']['rendered'],
                        'featured_media' => $item['guid']['rendered'],
                        'formattedDate' => $item['formattedData']
                    ]
                );
                Log::info('Preparing to save ActivitieJob: ', $item);
            }
        } catch (\Exception $e) {
            Log::error('Error in FetchActivitieJob: ' . $e->getMessage());
        }
    }
}
