<?php

namespace App\Jobs;

use App\Services\newsService;
use App\Models\WpPr\News;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Facades\Log;

class FetchNewsJob implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    protected $newsService;

    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            // resolve Service แบบนี้ก็ได้
            $newsService = app(NewsService::class);
            $newsData = $newsService->showDate();
            // ทำงานเหมือนเดิม
            foreach ($newsData as $item) {
                // ตรวจสอบข้อมูลก่อนบันทึก
                if (isset($item['id'], $item['slug'], $item['status'], $item['link'], $item['title']['rendered'], $item['formattedDate'])) {
                    News::updateOrCreate(
                        ['api_id' => $item['id']],
                        [
                            'slug' => $item['slug'],
                            'status' => $item['status'],
                            'link' => $item['link'],
                            'title' => $item['title']['rendered'],
                            'featured_media' => $item['featured_media'] ?? null, // ตรวจสอบค่าที่เป็นไปได้ว่า null ได้ไหม
                            'formattedDate' => $item['formattedDate']
                        ]
                    );
                    Log::info('Preparing to save NewsJob: ', $item);
                } else {
                    // ถ้าข้อมูลไม่ครบ ให้บันทึก error ลง log
                    Log::warning('NewsJob Incomplete news item data', ['item' => $item]);
                }
            }
        } catch (\Exception $e) {
            Log::info("message");
            ('Error in ProcessNewsJob: ' . $e->getMessage());
        }
    }
}
