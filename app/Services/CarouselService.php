<?php

// app/Services/CarouselService.php
namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

use App\Models\Carousel;

class carouselService
{

    public function CarouselFirstApi()
    {
        return Cache::remember(
            'carousels',
            60,
            function () {
                $response = Http::get('http://fms.kpru.ac.th/pr-fms/wp-json/wp/v2/dt_gallery/?dt_gallery_category=29&_fields%5B%5D=id&_fields%5B%5D=title&_fields%5B%5D=slug&_fields%5B%5D=status&_fields%5B%5D=link&_fields%5B%5D=featured_media');
                return $response->json();
            }
        );
    }
    public function CarouselSecondApi($variable)
    {
        $url = "http://fms.kpru.ac.th/pr-fms/wp-json/wp/v2/media/";
        $response = Http::get($url . $variable . '?&_fields[]=id&_fields[]=guid');

        if ($response->successful()) {
            return $response->json();
        }

        throw new \Exception('ไม่สามารถดึง API ที่อยู่รูปภาพได้');
    }
    public function fetchData()
    {
        // ดึงข้อมูลจาก API แรก
        $data = $this->CarouselFirstApi();
        $carouselData = [];
        foreach ($data as $item) {
            $secondApiData = $this->CarouselSecondApi($item['featured_media']);
            $carouselData[] = array_merge($item, $secondApiData);
        }
        return $carouselData;
    }
}
