<?php

// app/Services/NewsletterService.php
namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class NewsletterService
{

    public function NewsletteFirstApi()
    {
        return Cache::remember(
            'newslette',
            60,
            function () {
                $response = Http::get('https://fms.kpru.ac.th/pr-fms/wp-json/wp/v2/dt_portfolio/?dt_portfolio=48&_fields%5B%5D=id&_fields%5B%5D=title&_fields%5B%5D=slug&_fields%5B%5D=status&_fields%5B%5D=link&_fields%5B%5D=featured_media&_fields%5B%5D=date');
                return $response->json();
            }
        );
    }
    public function NewsletteSecondApi($variable)
    {
        $url = "http://fms.kpru.ac.th/pr-fms/wp-json/wp/v2/media/";
        $response = Http::get($url . $variable . '?&_fields[]=id&_fields[]=guid');

        if ($response->successful()) {
            return $response->json();
        }

        throw new \Exception('ไม่สามารถดึง API ที่อยู่รูปภาพได้');
    }
    public function NewslettefetchData()
    {
        // ดึงข้อมูลจาก API แรก
        $data = $this->NewsletteFirstApi();
        $carouselData = [];
        foreach ($data as $item) {
            $secondApiData = $this->NewsletteSecondApi($item['featured_media']);
            $NewsletteData[] = array_merge($item, $secondApiData);
        }
        return $NewsletteData;
    }
}
