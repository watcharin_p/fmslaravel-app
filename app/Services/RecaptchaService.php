<?php

namespace App\Services;

use Anhskohbo\NoCaptcha\NoCaptcha;

class RecaptchaService
{
    protected $captcha;

    public function __construct(NoCaptcha $captcha)
    {
        $this->captcha = $captcha;
    }

    /**
     * Verify the reCAPTCHA token.
     *
     * @param string $token
     * @return bool
     */
    public function verify(string $token): bool
    {
        return $this->captcha->verifyResponse($token);
    }
}
