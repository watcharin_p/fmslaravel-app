<?php

// app/Services/NewsService.php
namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class newsService
{
    public function newDataApi()
    {
        return Cache::remember('news', 60, function () {
            $response = Http::get('http://fms.kpru.ac.th/pr-fms/wp-json/wp/v2/posts?categories=57&_fields%5B%5D=id&_fields%5B%5D=title&_fields%5B%5D=slug&_fields%5B%5D=status&_fields%5B%5D=link&_fields%5B%5D=featured_media&_fields%5B%5D=date');
            if ($response->successful()) {
                return $response->json();
            }
            throw new \Exception('ไม่สามารถดึง API ข่าวได้');
        });
    }
    public function ShowDate()
    {
        $showdate = $this->newDataApi();
        $monthsThai = [
            1 => 'มกราคม',
            2 => 'กุมภาพันธ์',
            3 => 'มีนาคม',
            4 => 'เมษายน',
            5 => 'พฤษภาคม',
            6 => 'มิถุนายน',
            7 => 'กรกฎาคม',
            8 => 'สิงหาคม',
            9 => 'กันยายน',
            10 => 'ตุลาคม',
            11 => 'พฤศจิกายน',
            12 => 'ธันวาคม'
        ];

        $formattedData = array_map(function ($showdate) use ($monthsThai) {
            if (isset($showdate['date'])) {
                $date = Carbon::parse($showdate['date']);
                $day = $date->format('d');
                $month = $monthsThai[$date->month];
                $year = $date->year + 543;

                $showdate['formattedDate'] = "{$day} {$month} {$year}";
            }
            return $showdate;
        }, $showdate);

        return $formattedData;
    }
}
