<?php
// app/Services/ActivitieService.php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class activitieService
{
    public function ActivitieFirstApi()
    {
        return Cache::remember(
            'activities',
            60,
            function () {
                $response = Http::get('http://fms.kpru.ac.th/pr-fms/wp-json/wp/v2/dt_gallery/?dt_gallery_category=24,25,26,27&_fields%5B%5D=id&_fields%5B%5D=title&_fields%5B%5D=slug&_fields%5B%5D=status&_fields%5B%5D=link&_fields%5B%5D=featured_media&_fields%5B%5D=date&_fields[]=excerpt');
                return $response->json();
            }
        );
    }
    public function ShowDate()
    {
        $showdate = $this->ActivitieFirstApi();
        $monthsThai = [
            1 => 'มกราคม',
            2 => 'กุมภาพันธ์',
            3 => 'มีนาคม',
            4 => 'เมษายน',
            5 => 'พฤษภาคม',
            6 => 'มิถุนายน',
            7 => 'กรกฎาคม',
            8 => 'สิงหาคม',
            9 => 'กันยายน',
            10 => 'ตุลาคม',
            11 => 'พฤศจิกายน',
            12 => 'ธันวาคม'
        ];
        $formattedData = array_map(function ($showdate) use ($monthsThai) {
            if (isset($showdate['date'])) {
                $date = Carbon::parse($showdate['date']);
                $day = $date->format('d');
                $month = $monthsThai[$date->month];
                $year = $date->year + 543;
                $showdate['formattedData'] =
                    "{$day} {$month} {$year}";
            }
            return $showdate;
        }, array: $showdate);
        return $formattedData;
    }
    public function ActivitieSecondApi($variable)
    {
        $url = "http://fms.kpru.ac.th/pr-fms/wp-json/wp/v2/media/";
        $response = Http::get($url . $variable . '?&_fields[]=id&_fields[]=guid');

        if ($response->successful()) {
            return $response->json();
        }

        throw new \Exception('ไม่สามารถดึง API ที่อยู่รูปภาพได้');
    }
    public function fetchData()
    {
        // ดึงข้อมูลจาก API แรก
        $data = $this->ShowDate();
        $activitieData = [];
        foreach ($data as $item) {
            $secondApiData = $this->ActivitieSecondApi($item['featured_media']);
            $activitieData[] = array_merge($item, $secondApiData);
        }
        return $activitieData;
    }
}
