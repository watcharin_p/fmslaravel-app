<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Jobs\FetchCarouselJob;
use App\Services\CarouselService;

use App\Jobs\FetchNewsJob;
use App\Services\NewsService;

use App\Jobs\FetchActivitieJob;
use App\Services\ActivitieService;

class FetchCarouselNewsActivities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fetch-carousel-news-activities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // ดึงข้อมูลจาก Carousel
        FetchCarouselJob::dispatchSync(app(CarouselService::class));
        // ดึงข้อมูลจาก News
        FetchNewsJob::dispatchSync(app(NewsService::class));
        // ดึงข้อมูลจาก Activities
        FetchActivitieJob::dispatchSync(app(ActivitieService::class));

        $this->info('Carousel, News & Activities data fetching jobs dispatched!');
    }
}
