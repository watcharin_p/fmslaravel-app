<?php

namespace App\Console;

use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\FetchCarouselNewsActivities;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // ลงทะเบียนคำสั่งที่คุณสร้างขึ้นที่นี่
        \App\Console\Commands\FetchCarouselNewsActivities::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // ตั้งเวลาการทำงานของคำสั่งที่นี่
        // ตัวอย่าง:
        $schedule->command('fetch:carousel-news-activities')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        // คุณสามารถลงทะเบียนคำสั่งอื่น ๆ ที่นี่ได้ด้วย
        $this->commands([
            FetchCarouselNewsActivities::class,
        ]);
    }
}
