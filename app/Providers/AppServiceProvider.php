<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // สร้าง nonce
        $nonce = base64_encode(random_bytes(16));

        // แชร์ nonce ให้กับทุก view
        View::share('nonce', $nonce);
    }
}
