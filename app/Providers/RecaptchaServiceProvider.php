<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\RecaptchaService;
use Anhskohbo\NoCaptcha\NoCaptcha;

class RecaptchaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        // Retrieve the secret and sitekey from the config/captcha.php file
        $this->app->singleton(RecaptchaService::class, function ($app) {
            $secret = config('captcha.secret');
            $sitekey = config('captcha.sitekey');
            return new RecaptchaService(new NoCaptcha($secret, $sitekey));
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
