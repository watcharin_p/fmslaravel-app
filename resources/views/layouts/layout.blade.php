<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="scroll-smooth">
<head>
    <!-- HTML Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/favicon.ico" sizes="any">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="canonical" href="https://fms.kpru.ac.th/">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Dev.FMS@KPRU">
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link
        href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;400;500&family=Roboto:ital,wght@0,300;0,500;1,400&display=swap"
        rel="stylesheet" />
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-544EN5K8J0"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'G-544EN5K8J0');
    </script>
    <!-- END Google tag (gtag.js) -->
    <!-- google-site-verification -->
    <meta name="google-site-verification" content="wntUckqhYNiFTL8jFb4VsnLzFjcHT6dC_6YhlF3OFew" />
    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WCWXTT6');
    </script>
    <!-- End Google Tag Manager -->
    <meta name="facebook-domain-verification" content="3ujmzi3rzwf1mm46atd9u41rznrhlz" />
    @yield('head')
</head>
<body class="font-sans antialiased">
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WCWXTT6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @yield('content')

   <!-- PDPA Consent Card Form -->
    {{-- @if(!Cookie::get('pdpa_consent')) --}}
    {{-- <div id="pdpa-consent-card" class="fixed z-50 w-3/4 p-8 transform -translate-x-1/2 rounded-lg shadow-lg lg:w-2/4 bottom-1 bg-white/80 left-1/2 right-32"> --}}
        {{-- <div class="flex items-center justify-end mx-auto"> --}}
            {{-- <p class="text-sm">เว็บไซต์นี้ใช้คุกกี้เพื่อมอบประสบการณ์ที่ดีที่สุด กรุณายอมรับข้อกำหนดความเป็นส่วนตัวของ <a class="text-primary-500" href="https://www.kpru.ac.th/pdpa/" target="_blank">PDPA@KPRU</a></p> --}}
            {{-- <form method="POST" action="{{ route('pdpa.accept') }}"> --}}
                {{-- @csrf --}}
                {{-- <button type="submit" class="px-4 py-2 ml-2 text-white bg-blue-600 rounded-lg hover:bg-blue-700"> --}}
                    {{-- ยอมรับ --}}
                {{-- </button> --}}
            {{-- </form> --}}
            <!-- ปุ่มปิด -->
            {{-- <button id="close-pdpa-card" class="px-4 py-2 ml-2 text-white bg-red-600 rounded-lg hover:bg-red-700">ปิด</button> --}}
        {{-- </div> --}}
    {{-- </div> --}}
    {{-- <script>
        // JavaScript สำหรับปิด PDPA Card
        document.getElementById('close-pdpa-card').addEventListener('click', function() {
            document.getElementById('pdpa-consent-card').style.display = 'none';
        });
    </script>
    @endif --}}

    <!-- ส่งค่าภาษาไป Vue -->
    <script>
        window.i18n = @json(trans('messages'));
        window.locale = "{{ app()->getLocale() }}";
    </script>

    <!-- Cookie Consent by https://www.cookiewow.com -->
    <script type="text/javascript" src="https://cookiecdn.com/cwc.js"></script>
    <script id="cookieWow" type="text/javascript" src="https://cookiecdn.com/configs/5D4uZoAnBxvAyNLstRgDjwyS"
    data-cwcid="5D4uZoAnBxvAyNLstRgDjwyS"></script>
</body>
</html>
