
@extends('layouts.layout')

@section('head')
    <title>สายตรงคณบดี - คณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร</title>

    <!-- HTML Meta Tags -->
    <meta name="description"
        content="มหาวิทยาลัยราชภัฏกำแพงเพชร เป็นมหาวิทยาลัยที่เป็นที่พึ่งทางวิชาการของท้องถิ่นให้มีความเข้มแข็งอย่างยั่งยืน สถาปนาเมื่อ ปี 2516 ปัจจุบันประกอบด้วย 5 คณะ">

    <!-- Keywords Meta Tags -->
    <meta name="Keywords" lang="en"
        content="Covid, Kamphaeng Phet Rajabhat University, Rajabhat, KamphaengPhet, KPRU, KP, Kamphaeng, Thailand, Thai University">
    <meta name="Keywords" lang="th"
        content="มหาวิทยาลัยราชภัฏกำแพงเพชร, ราชภัฏกำแพงเพชร, มรภ.กพ., วิทยาลัยครูกำแพงเพชร, วิทยาลัยครู, ราชภัฏภาคเหนือ, มหาวิทยาลัยราชภัฏ, มหาวิทยาลัย, สถาบันการศึกษา">

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="หน้าแรก | คณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร">
    <meta itemprop="description"
        content="คณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร เป็นศูนย์กลางในการพัฒนาองค์ความรู้ด้านการบริหารจัดการ โดยการบูรณาการทางวิชาการ วิชาชีพ เพื่อการพัฒนาชุมชนท้องถิ่นของมหาวิทยาลัยราชภัฏภาคเหนือตอนล่าง">
    <meta itemprop="image" content="/storage/fb_og_image.jpg">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://fms.kpru.ac.th">
    <meta property="og:type" content="website">
    <meta property="og:title" content="หน้าแรก | คณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร">
    <meta property="og:description"
        content="คณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร เป็นศูนย์กลางในการพัฒนาองค์ความรู้ด้านการบริหารจัดการ โดยการบูรณาการทางวิชาการ วิชาชีพ เพื่อการพัฒนาชุมชนท้องถิ่นของมหาวิทยาลัยราชภัฏภาคเหนือตอนล่าง">
    <meta property="og:image" content="/storage/fb_og_image.jpg">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="หน้าแรก | คณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร">
    <meta name="twitter:description"
        content="คณะวิทยาการจัดการ มหาวิทยาลัยราชภัฏกำแพงเพชร เป็นศูนย์กลางในการพัฒนาองค์ความรู้ด้านการบริหารจัดการ โดยการบูรณาการทางวิชาการ วิชาชีพ เพื่อการพัฒนาชุมชนท้องถิ่นของมหาวิทยาลัยราชภัฏภาคเหนือตอนล่าง">
    <meta name="twitter:image" content="/storage/fb_og_image.jpg">
    <link rel="icon" href="/favicon.ico" sizes="any">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">

    @vite(['resources/css/app.css'])

    <script src="https://www.google.com/recaptcha/api.js?render={{ config('captcha.sitekey') }}"></script>
@endsection

@section('content')
    <!-- FMS | Dean Page -->
    <section id="Form-Dean">
        <div class="container mx-auto ">
            <div class="mx-auto my-10 bg-red-400 rounded-lg shadow-lg">
                <div class="p-10">
                    <div class="grid grid-cols-2 gap-5">
                        <div>
                            <div class="bg-white rounded-md">
                                <h1 class="p-4 text-2xl font-bold">สายตรงคณบดี</h1>
                                <h2 class="px-4 text-xl font-bold">วัตถุประสงค์</h2>
                                <ul class="p-4 text-lg list-disc list-inside">
                                    <li class="p-2">รับข้อเสนอความคิดเห็นในการพัฒนา และปรับปรุงหน่วยงาน</li>
                                    <li class="p-2">รับฟังเสียงสะท้อนหรือข้อร้องเรียนของผู้รับบริการ ผู้มาติดต่อ
                                        หรือผู้มีส่วนได้ส่วนเสียหรือสาธารณชน เช่น การทุจริตของเจ้าหน้าที่ในหน่วยงาน
                                        การจัดซื้อจัดจ้าง ความไม่เป็นธรรมในการให้บริการ การรับสินบน
                                        ความไม่โปร่งใสต่อการดำเนินการภายในองค์กร เป็นต้น</li>
                                    <p class="p-2">ทั้งนี้ การร้องเรียนการทุจริตและประพฤติมิชอบของบุคลากรภายในหน่วยงาน
                                        ถือเป็นความลับทางราชการ มหาวิทยาลัยและผู้รับผิดชอบ จะปกปิดชื่อผู้ร้องเรียน
                                        และข้อมูลที่เกี่ยวข้องเป็นความลับ
                                        โดยคำนึงถึงความปลอดภัยและความเสียหายของทุกฝ่ายที่เกี่ยวข้อง</p>
                                </ul>
                                <h3 class="ml-4 font-bold">ขั้นตอนและกระบวนการ</h3>
                                <ul class="p-4 text-lg list-disc list-inside">
                                    <li>รับเรื่อง</li>
                                    <li>แยกประเภทความคิดเห็น</li>
                                    <li>ดำเนินการตรวจสอบ</li>
                                    <li>รายงานผล</li>
                                    <li>แจ้งผลการดำเนินการ</li>
                                </ul>
                            </div>
                        </div>
                        <div class="relative">
                            @if(session('success'))
                            <span class="text-green-600">{{ session('success') }}</span>
                            @endif
                            @error('g-recaptcha-response')
                                    <span class="text-red-600">{{ $message }}</span>
                            @enderror
                            <form class="p-4 mx-auto" id="DeanForm" action="{{ route('dean.store') }}" method="POST" >
                                @csrf
                                <div class="mb-5 ">
                                    <label id="fullname"
                                        class="block mb-2 text-sm font-medium text-gray-900">ชื่อ -
                                        นามสกุล</label>
                                    <input type="text" name="fullname"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                       placeholder="ชื่อ-นามสกุล" required />
                                </div>
                                <div class="mb-5">
                                    <label id="phone"
                                        class="block mb-2 text-sm font-medium text-gray-900">เบอร์โทร</label>
                                    <input type="tel" name="phone"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                        placeholder="เบอร์ติดต่อ"required />
                                </div>
                                <div class="mb-5">
                                    <label id="email"
                                        class="block mb-2 text-sm font-medium text-gray-900">อีเมล</label>
                                    <input type="email" name="email"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                        placeholder="อีเมล" required />
                                </div>
                                <div class="mb-5">
                                    <label id="subject"
                                        class="block mb-2 text-sm font-medium text-gray-900">หัวข้อ</label>
                                    <input type="text" name="subject"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                                        placeholder="ชื่อเรื่อง" required>
                                </div>
                                <label id="message"
                                    class="block mb-2 text-sm font-medium text-gray-900">ข้อความ</label>
                                <textarea name="message" rows="4"
                                    class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"
                                    placeholder="ข้อความ" required></textarea>

                                <button class="box-border w-full h-10 my-5 text-white bg-blue-800 g-recaptcha" data-sitekey="{{ config('captcha.sitekey') }}" data-callback='onSubmit' data-action='submit'>ส่ง</button>

                            </form>
                            <div class="right-0 w-full m-4 ">
                                <a class="h-10 p-4 bg-white" href="{{ route('home') }}">
                                    กลับไปหน้าแรก
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
    function onSubmit(token) {
     document.getElementById("DeanForm").submit();
   }
</script>
@endsection
