@extends('layouts.layout')

@section('head')
    <title inertia>{{ config('app.name', 'FMSLaravel') }}</title>
    <!-- Scripts -->
    @routes
    @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
    @inertiaHead
@endsection

@section('content')
    <!-- FMS | Home Page -->
    @inertia
@endsection

