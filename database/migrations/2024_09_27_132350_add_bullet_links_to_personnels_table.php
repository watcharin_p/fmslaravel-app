<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('personnels', function (Blueprint $table) {
            $table->string('bullet_link1')->nullable();
            $table->string('bullet_link2')->nullable();
            $table->string('bullet_link3')->nullable();
            $table->string('bullet_link4')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('personnels', function (Blueprint $table) {
            $table->dropColumn(['bullet_link1', 'bullet_link2', 'bullet_link3', 'bullet_link4']);
        });
    }
};
