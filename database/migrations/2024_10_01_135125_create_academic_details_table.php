<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('academic_details', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id');
            $table->string('research_project_id')->nullable();
            $table->string('publication_title')->nullable();
            $table->string('publication_type_name_tha')->nullable();
            $table->string('publication_level_name_tha')->nullable();
            $table->string('county_name_tha')->nullable();
            $table->float('member_action_percent')->nullable();
            $table->string('member_action_type_name_tha')->nullable();
            $table->text('publication_owner_all')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('academic_details');
    }
};
