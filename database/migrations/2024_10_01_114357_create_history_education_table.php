<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('history_education', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id');
            $table->string('institute_name')->nullable();;
            $table->string('curriculum_name_tha')->nullable();
            $table->string('certificate_name')->nullable();;
            $table->string('program_name_th')->nullable();;
            $table->string('graduate_year')->nullable();;
            $table->string('education_level_name')->nullable();;
            $table->string('country_name_tha')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('history_education');
    }
};
