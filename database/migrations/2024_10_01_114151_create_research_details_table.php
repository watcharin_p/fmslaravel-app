<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('research_details', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id');
            $table->string('research_project_id');
            $table->string('project_continue_type_name_tha');
            $table->string('research_project_name_tha');
            $table->string('project_research_member_action_name_tha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('research_details');
    }
};
