<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('personnels', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id');
            $table->string('prename_full_tha');
            $table->string('first_name_tha');
            $table->string('last_name_tha');
            $table->string('employee_name');
            $table->string('office_phone')->nullable();
            $table->string('work_status_name');
            $table->string('employee_type_name');
            $table->string('sunit_name');
            $table->string('position_rank_name');
            $table->string('picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('personnels');
    }
};
