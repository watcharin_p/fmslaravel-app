<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pr_activities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('api_id')->unique(); // บันทึก id จาก API
            $table->string('slug');
            $table->string('status');
            $table->string('link');
            $table->string('title');
            $table->string('featured_media')->nullable();
            $table->text('excerpt');
            $table->string('formattedDate'); // วันที่ format แล้ว
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pr_activities');
    }
};
