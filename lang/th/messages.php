<?php

return [
    // Main Messages TH
    'welcome' => 'ยินดีต้อนรับสู่เว็บไซต์ของเรา!',
    'department' => 'คณะวิทยาการจัดการ',
    'university' => 'มหาวิทยาลัยราชภัฏกำแพงเพชร'
];
