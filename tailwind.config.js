import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
        './node_modules/flowbite/**/*.js',
    ],

    theme: {
        extend: {
            fontFamily: {
      'body': [
        'Kanit',
        'Roboto',
        // other fallback fonts
      ],
      'sans': [
        'Kanit',
        'Roboto',
        // other fallback fonts
      ]
    },
            colors: {
                primary: {"50":"#fdf2f8","100":"#fce7f3","200":"#fbcfe8","300":"#f9a8d4","400":"#f472b6","500":"#ec4899","600":"#db2777","700":"#be185d","800":"#9d174d","900":"#831843","950":"#500724"}
            },
            backgroundImage: {
                'footer-texture': "url('/images/footer-transparent-bg.webp')",
            },
        },
    },

    plugins: [
        forms,
        require('flowbite/plugin')
    ],
};
