<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ComplaintController;
use App\Http\Controllers\Department\DepartmentController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\PersonnelController;
use App\Http\Controllers\PDPAConsentController;
use App\Http\Controllers\EventController;

use App\Http\Controllers\Curriculun\CurriculumController;

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SitemapController;

// use App\Http\Controllers\PostController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

// lang
Route::get('lang/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'th'])) {
        abort(400);
    }
    session(['locale' => $locale]);
    return redirect()->back();
});

// เว็บไซต์เพจหลัก
//
Route::get('/welcome', [HomeController::class, 'index'])->name('welcome');
//
Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('about', [HomeController::class, 'about'])->name('about');
Route::get('services', [HomeController::class, 'service'])->name('service');
Route::get('fmsbooking', [HomeController::class, 'fmsbooking'])->name('fmsbooking');
Route::get('contact-us', [HomeController::class, 'contact'])->name('contact');
Route::get('student', [StudentController::class, 'index'])->name('student');
Route::get('personnel', [PersonnelController::class, 'index'])->name('personnel');
Route::get('personnel/{employee_id}', [PersonnelController::class, 'show'])->name('personel.show');
Route::get('research', [HomeController::class, 'Research'])->name('research');
Route::get('journal', [HomeController::class, 'Journal'])->name('journal');
Route::get('dean', [ComplaintController::class, 'index'])->name('dean');
Route::post('dean/store', [ComplaintController::class, 'store'])->name('dean.store');

// แนะนำ
Route::prefix('introduction')->group(function () {
    //  URL : "/introduction"
    Route::get('/', [HomeController::class, 'introduction'])->name('introduction');
    //  URL : "/introduction/university"
    Route::get('/university', [HomeController::class, 'University'])->name('university');
    //  URL : "/introduction/faculty"
    Route::get('/faculty', [HomeController::class, 'Faculty'])->name('faculty');
    //  URL : "/introduction/university-atmosphere"
    Route::get('/university-atmosphere', [HomeController::class, 'UniversityAtmosphere'])->name('university-atmosphere');
    //  URL : "/introduction/faculty-atmosphere"
    Route::get('/faculty-atmosphere', [HomeController::class, 'FacultyAtmosphere'])->name('faculty-atmosphere');
});


// Event Page
Route::prefix('event')->group(function () {
    Route::get(
        '/',
        [EventController::class, 'index']
    )->name('event');
    // URL : "/event"

    Route::prefix(
        'NACM14th'
    )->group(function () {
        Route::get('/', [EventController::class, 'NACM14th'])->name('NACM14th');
        // URL : "/event/NACM14th"

        // Route::get(
        //     '/opening-nacm14th',
        //     [EventController::class, 'openingnacm14th']
        // )->name('nacm14th');
        // URL : "/event/opening-nacm14th"
        // Route::get(
        //     '/rov-fms',
        //     [EventController::class, 'rovfms']
        // )->name('rovfms');
        // URL : "/event/rov-fms"

    });
});

// url สร้าง(fetch) เฉพาะใน localhost only
Route::get('/fetch-personnel-fms', [PersonnelController::class, 'fetchAndStore']);
Route::get('/fetch-employee/{employee_id}', [PersonnelController::class, 'featchEmployeeDetail']);
// TEST PAGE
Route::get('test', [TestController::class, 'index'])->name('test');

//admin สายตรง
Route::get('complaint', [ComplaintController::class, 'view'])->middleware(['auth', 'verified'])->name('complaint');

// เว็บไซต์เพจข้อมูลหลักสูตร
// หลักสูตรปริญญาตรี
Route::prefix('undergraduate')->group(function () {
    Route::get('/', [CurriculumController::class, 'undergraduate'])->name('undergraduate');
    // URL : "/undergraduate"
    Route::get('/accounting', [CurriculumController::class, 'CurriculumAccounting'])->name('curriculum-accounting');
    // URL : "/undergraduate/accounting"
    Route::get(
        '/business-administration',
        [CurriculumController::class, 'CurriculumBusinessadministration']
    )->name('curriculum-business-administration');
    // URL : "/undergraduate/business-administration"
    Route::get(
        '/business-management-major',
        [CurriculumController::class, 'CurriculumBusinessManagementMajor']
    )->name('curriculum-business-management-major');
    // URL : "/undergraduate/business-management-major"
    Route::get(
        '/marketing-major',
        [CurriculumController::class, 'CurriculumMarketingMajor']
    )->name('curriculum-marketing-major');
    // URL : "/undergraduate/marketing-major"
    Route::get(
        '/finance-and-banking-major',
        [CurriculumController::class, 'CurriculumFinanceAndBankingMajor']
    )->name('curriculum-finance-and-banking-major');
    // URL : "/undergraduate/finance-and-banking-major"
    Route::get(
        '/digital-business-technology-major',
        [CurriculumController::class, 'CurriculumDigitalBusinessTechnologyMajor']
    )->name('curriculum-digital-business-technology-major');
    // URL : "/undergraduate/digital-business-technology-major"
    Route::get(
        '/entrepreneurship-major',
        [
            CurriculumController::class,
            'CurriculumEntrepreneurshipMajor'
        ]
    )->name('curriculum-entrepreneurship-major');
    // URL : "/undergraduate/entrepreneurship-major"
    Route::get(
        '/tourism-industry',
        [
            CurriculumController::class,
            'CurriculumTourismIndustry'
        ]
    )->name('curriculum-tourism-industry');
    // URL : "/undergraduate/tourism-industry"
    Route::get(
        '/communication-arts',
        [
            CurriculumController::class,
            'CurriculumCommunicationArts'
        ]
    )->name('curriculum-communication-arts');
    // URL : "/undergraduate/communication-arts"
});
// หลักสูตรปริญญาโท
Route::prefix('mastergraduate')->group(function () {
    Route::get('/', [CurriculumController::class, 'mastergraduate'])->name('mastergraduate');
    // URL : "/mastergraduate"
    Route::get(
        '/mba',
        [CurriculumController::class, 'CurriculumMBA']
    )->name('curriculum-mba');
    // URL : "/mastergraduate/mba"
});

//Department Route เว็บไซต์เพจของสาขาวิชา
Route::get(
    '/accounting',
    [DepartmentController::class, 'accounting']
)->name('accounting');
// URL : "/accounting" เว็บไซต์สาขาวิชาการบัญชี

Route::get(
    '/business-management-major',
    [DepartmentController::class, 'BusinessManagementMajor']
)->name('business-management-major');
// URL : "/business-management-major" เว็บไซต์สาขาวิชาการจัดการธุรกิจ

Route::get(
    '/marketing-major',
    [DepartmentController::class, 'MarketingMajor']
)->name('marketing-major');
// URL : "/marketing-major" เว็บไซต์สาขาวิชาการตลาด

Route::get(
    '/finance-and-banking-major',
    [DepartmentController::class, 'FinanceAndBankingMajor']
)->name('finance-and-banking-major');
// URL : "/finance-and-banking-major" เว็บไซต์สาขาวิชาการเงินและการธนาคาร

Route::get(
    '/digital-business-technology-major',
    [DepartmentController::class, 'DigitalBusinessTechnologyMajor']
)->name('digital-business-technology-major');
// URL : "/digital-business-technology-major"  เว็บไซต์สาขาวิชาเทคโนโลยีธุรกิจดิจิทัล

Route::get(
    '/entrepreneurship-major',
    [DepartmentController::class, 'EntrepreneurshipMajor']
)->name('entrepreneurship-major');
// URL : "/entrepreneurship-major" เว็บไซต์สาขาวิชาการเป็นผู้ประกอบการ

Route::get(
    '/tourism-industry',
    [DepartmentController::class, 'TourismIndustry']
)->name('tourism-industry');
// URL : "/tourism-industry" เว็บไซต์สาขาวิชาอุตสาหกรรมการท่องเที่ยว

Route::get(
    '/communication-arts',
    [DepartmentController::class, 'CommunicationArts']
)->name('communication-arts');
// URL : "/communication-arts" เว็บไซต์สาขาวิชานิเทศศาสตร์

Route::get(
    '/mba',
    [DepartmentController::class, 'MBA']
)->name('mba');
// URL : "/mba" เว็บไซต์สาขาวิชาการจัดการสมัยใหม่

// Department Route เว็บไซต์เพจของหน่วยงานย่อย

Route::get(
    '/research-innovation',
    [DepartmentController::class, 'ResearchInnovation']
)->name('research-innovation');
// URL : "/research-innovation" ฝ่ายงานวิจัยและนวัตกรรม

Route::get(
    '/academic-services',
    [DepartmentController::class, 'AcademicServices']
)->name('academic-services');
// URL : "/academic-services" ฝ่ายงานบริการวิชาการ

Route::get(
    '/plan-develop',
    [DepartmentController::class, 'PlanDevelop']
)->name('plan-develop');
// URL : "/plan-develop"  ฝ่ายงานแผนและพัฒนา

Route::get(
    '/academic-work',
    [DepartmentController::class, 'AcademicWork']
)->name('academic-work');
// URL : "/academic-work" ฝ่ายงานวิชาการ

Route::get(
    '/knowledge-management',
    [DepartmentController::class, 'KnowledgeManagement']
)->name('knowledge-management');
// URL : "/knowledge-management" ฝ่ายงานการจัดการความรู้

Route::get(
    '/student-affairs',
    [DepartmentController::class, 'StudentAffairs']
)->name('student-affairs');
// URL : "/student-affairs" ฝ่ายกิจการนักศึกษา

Route::get(
    '/internship-services-center',
    [DepartmentController::class, 'InternshipServices']
)->name('internship-services');
// URL : "/internship-services-center" ศูนย์ฝึกประสบการณ์วิชาชีพ/สหกิจศึกษา

Route::get(
    '/human-resource-development-center',
    [DepartmentController::class, 'HumanResourceDevelopment']
)->name('human-resource-development');
// URL : "/human-resource-development-center" ศูนย์พัฒนาศักยภาพทรัพยากรมนุษย์

Route::get(
    '/alumni-association',
    [DepartmentController::class, 'AlumniAssociation']
)->name('alumni-association');
// URL : "/alumni-association" ศิษย์เก่าและสมาคมศิษย์เก่า

Route::middleware('pdpa')->group(function () {
    //เส้นทางที่ต้องการให้ผู้ใช้ยอมรับ PDPA ก่อนเข้าใช้งาน
});

// pdpa Consent
Route::get('/pdpa-consent', [PDPAConsentController::class, 'showConsentForm'])->name('pdpa.consent');
Route::post('/pdpa-consent/accept', [PDPAConsentController::class, 'acceptConsent'])->name('pdpa.accept');
Route::get('/pdpa-consent/reset', [PDPAConsentController::class, 'resetConsent'])->name('pdpa.reset');

// Root Auth Page
Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

// Genrate URL
Route::get('generate-sitemap', [SitemapController::class, 'generateSitemap']);
Route::get('/sitemap.xml', function () {
    return response()->file(public_path('sitemap.xml'));
});

// ERROR URL
Route::fallback(function () {
    return Inertia::render('Error/404');
});
// ERROR TEST 500
Route::get('/force-error', function () {
    abort(500);
});

require __DIR__ . '/auth.php';
